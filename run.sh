#!/bin/bash

# Clone the starlingx/tools repository.

yum -y install git

if ! git clone https://opendev.org/starlingx/tools; then
    echo "Cannot clone the repository"
    exit 1
fi

# Create docker image for mirror check

pushd tools/centos-mirror-tools

rm /etc/yum.repos.d/CentOS-Sources.repo

cp yum.repos.d/* /etc/yum.repos.d

./mirror-check.sh
